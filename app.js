require('app-module-path').addPath(__dirname);
var express = require('express');
var cors = require('cors');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');
var mongoose = require('mongoose');
mongoose.connect('mongodb://db');
var MongoStore = require("connect-mongo")(session);
var app = express();

app.use(session({
	secret: 'biscuits are delicious',
	resave: true,
	saveUninitialized: true,
	store: new MongoStore({
			mongooseConnection: mongoose.connection
		},
		function(err) {
			console.log("Mongostore error? ", err || "Mongo store setup fine");
		}),
	cookie: {
		maxAge: 60 * 1000 * 60,
		secure: true
	}
}));

var env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';


// app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cors({
	credentials: true,
	origin: true
}));
app.use(passport.initialize());
app.use(passport.session());
var auths = require('auths')(app, passport);
var routes = require('routes')(app, passport);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	console.log(err.message);
	err.status = 404;
	next(err);
});

/// error handlers

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		console.log(err);
		res.status(err.status || 500);
		res.send({
			message: err.message,
			error: err,
			title: 'error'
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.send({
		message: err.message,
		error: {},
		title: 'error'
	});
});


module.exports = app;
