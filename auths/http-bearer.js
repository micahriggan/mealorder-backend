var handler = function(app, passport) {
	var models = require('models');
	var BearerStrategy = require('passport-http-bearer').Strategy;


	passport.use(new BearerStrategy(function(token, done) {
		models.User.findOne({
			token: token
		}, function(err, user) {
			//console.log("bearer token", err, user);

			if (err) {
				return done(err);
			}
			if (!user) {
				return done(null, false);
			}


			return done(null, user, {
				scope: 'all'
			});
		});
	}));


};

module.exports = handler;
