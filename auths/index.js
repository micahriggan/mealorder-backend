var handler = function(app, passport) {
	var mongoose = require('mongoose');
	var models = require('models');


	//app.use(function(req, res, next) {
		//console.log("Headers ", req.headers);
		//next();
	//});

	var providers = {
		Google: require('auths/google')(app, passport),
		Local: require('auths/local')(app, passport),
		Bearer: require('auths/http-bearer')(app, passport),
	};


	passport.serializeUser(function(user, callback) {
		//console.log('serializing user.', user);
		callback(null, user);
	});

	passport.deserializeUser(function(user, callback) {
		//console.log('deserialize user.');
		models.User.findById(user.id, function(err, user) {
			console.log(err, user);
		});
		callback(null, user);
	});

	return providers;

};
module.exports = handler;
