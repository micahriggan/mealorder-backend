var handler = function(app, passport) {
	var models = require('models');
	var LocalStrategy = require('passport-local').Strategy;
	var crypto = require('crypto');

	passport.use(new LocalStrategy(function(username, password, done) {

		console.log("LocalStrategy", username);
		var token = crypto.randomBytes(64).toString('hex');
		console.log("token : ", token);
		models.User.findOneAndUpdate({
			username: username
		}, {
			token: token
		}, function(err, user) {

			if (err) {
				console.log("LocalError ", err);
				return done(err);
			}
			if (!user) {
				console.log("LocalError user not found");
				return done(null, false);
			}
			models.Password.checkPassword(username, password, function(match) {
				if (!match) return done(null, false);
				user.token = token;
				return done(err, user, {
					scope: 'all'
				});
			});
		});
	}));

	app.post('/auth/local/', passport.authenticate('local'), function(req, res) {
		res.json({
			token: req.user.token
		});
	});





};

module.exports = handler;
