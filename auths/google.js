var handler = function(app, passport) {
	var mongoose = require('mongoose');
	var models = require('models');
	var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

	//Google authentication
	// console.log("Deleting all users");
	// models.User.remove({}, function(err, model) {});
	//Delete all users for testing
	//

	passport.use(new GoogleStrategy({
			clientID: process.env.google_clientID,
			clientSecret: process.env.google_clientSecret,
			callbackURL: process.env.google_callbackURL
		},
		function(token, tokenSecret, profile, done) {
			console.log("Calling find or create for Google");
			process.nextTick(function() {
				return GoogleFindOrCreate(profile, function(err, user) {
					if (user) {
						user.token = token;
						user.save(function(err, saved) {
							done(err, saved);
						});
					} else {
						done(err, user);
					}
				});
			});

		}
	));


	var redirectTo;
	//querystring usage : redirects to refer
	//  if successful redirect to refer + successPage
	app.get('/auth/google/', function(req, res) {
		var ref = req.get('Referrer') || "";
		console.log(req.get('Referrer'), req.session);
		redirectTo = {
			success: ref + '#/' + (req.query.success || ""),
			fail: ref + (req.query.fail || "")
		};
		//after the login we need to redirect
		//if a successPage is given, append it.
		res.redirect('/auth/google/do');
	});

	app.get('/auth/google/do', passport.authenticate('google', {
		scope: ['profile', 'email'],
		failureRedirect: '/err'
	}));


	app.get('/auth/google/callback',
		passport.authenticate('google'),
		function(req, res) {
			if (redirectTo.success) {
				var url = redirectTo.success + "/" + req.user.token;
				console.log("Redirecting to " + url);
				res.redirect(url);
				//res.send(req.user);
			} else {
				res.redirect('/');
			}
		});




	function generateRefferal(data) {
		var firstName = data.name.givenName;
		var start = data.id.length > 4 ? data.id.length - 4 : 0;
		var GoogleReferral = firstName.replace(" ") + data.id.substring(start);
		console.log("Generated referral : " + GoogleReferral);
		return GoogleReferral;

	}

	function GoogleUser(data) {
		console.log(data);
		var firstname = data.name.givenName;
		var lastname = data.name.familyName;
		var GoogleReferral = generateRefferal(data);
		var email = data.emails[0].value;
		var username = email;
		var created = {
			profileId: data.id,
			firstname: firstname,
			email : email,
			username: username,
			lastname: lastname,
			referral: GoogleReferral
		};
		//console.log("Generated User : " + created);
		return created;
	}

	function GoogleFindOrCreate(profile, callback) {
		console.log("Entering GoogleFindOrCreate");
		models.User.findOne({
				profileId: profile.id
			},
			//Find the user. If no user, create User
			function(err, usr) {
				if (!usr) {
					// No user was found, create User
					var newUser = GoogleUser(profile);
					models.User.create(newUser, function(createErr, user) {
						console.log(createErr, user);
						return callback(createErr, user);
					});
				} else {
					return callback(err, usr);
				}
			}
		);
	}

};
module.exports = handler;
