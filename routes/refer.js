var handler = function(app, passport) {


	var express = require('express');
	var router = express.Router();
	var models = require('models');
	/* Get the current user's refer code */
	router.get('/', passport.authenticate('bearer'), function(req, res) {
		res.send(req.user.referral);
	});

	router.get('/check/:referral', function(req, res) {
		models.User.findOne({
			referral: req.params.referral
		}, function(err, user) {
			if (!err && user) {
				res.sendStatus(200);
			} else {
				res.sendStatus(500);
			}
		});
	});

	router.post('/:refferedBy', passport.authenticate('bearer'), function(req, res) {
		var refferedBy = req.params.refferedBy;
		console.log(refferedBy, req.user);
		if (!req.user.refferedBy && refferedBy) {
			var options = {
				new: true
			};
			var query = {
				_id: req.user._id
			};
			var update = {
				refferedBy: refferedBy
			};
			models.User.findOne({
				referral: refferedBy
			}, function(err, ref) {

				if (ref) {
					models.User.findOneAndUpdate(query, update, options, function(err, user) {
						if (user) {
							console.log(user);
							res.sendStatus(200);
						} else {
							res.sendStatus(304);
						}

					});
				} else {
					res.sendStatus(304);
				}

			});

		}
	});


	return router;

};

module.exports = handler;
