var handler = function(app, passport) {

	var express = require('express');
	var router = express.Router();
	var brainTree = require('braintree');
	var Models = require('models');
	var ObjectId = require('mongoose').Types.ObjectId;
	var _ = require('underscore');

	var brainTreeClient = brainTree.connect({
		environment: brainTree.Environment.Sandbox,
		merchantId: process.env.braintree_merchantid,
		publicKey: process.env.braintree_publickey,
		privateKey: process.env.braintree_privatekey,

	});

	function getBrainTreeUser(userModel, callback) {
		brainTreeClient.customer.find(userModel.customerId, callback);
	}


	function chargeSubscribedUser(user, mealsOrdered, callback) {
		Models.Order.GetOrderPrice(user, function(err, pricingInfo) {

			//console.log("pricingInfo ", pricingInfo, "mealsOrdered", mealsOrdered);
			var mealsToCharge = pricingInfo.subscriptionMealsLeft > mealsOrdered.length ? 0 : mealsOrdered.length - pricingInfo.subscriptionMealsLeft;
			var addonId = "";
			//get the correct addon id for the subscription
			if (user.planId == "Premium")
				addonId = "srhr";
			else if (user.planId == "Valued")
				addonId = "srhs";
			//console.log("Meals to charge : ", mealsToCharge, addonCreates);
			if (mealsToCharge > 0) {
				brainTreeClient.subscription.find(user.subscriptionId, function(err, sub) {
					var brainTreeAddons = {};
					var brainTreeCreate = {};
					var brainTreeUpdate = {};
					var currentAddon = _.findWhere(sub.addOns, {
						id: addonId
					});
					if (currentAddon) {
						brainTreeUpdate.existingId = currentAddon.id;
						brainTreeUpdate.quantity = currentAddon.quantity + 1;
						brainTreeAddons.update = [brainTreeUpdate];
					} else {
						brainTreeCreate.inheritedFromId = addonId;
						brainTreeAddons.add = [brainTreeCreate];
					}
					console.log("Creates/ Updates", brainTreeAddons);
					brainTreeClient.subscription.update(user.subscriptionId, {
						addOns: brainTreeAddons
					}, callback);

					//add instead of update

				});
			} else callback(null, false);


		});
	}
	router.post('/', passport.authenticate('bearer'), function(req, res) {
		var creates = req.body.order.items;
		var newOrder = req.body.order;
		newOrder.user = req.user._id;
		//Hard coding addonIds until the api allows me to find them
		if (req.user.planId && req.user.subscriptionId) {
			chargeSubscribedUser(req.user, creates, function(err, result) {
				console.log("Err res : ", err, result);
				if (!err) {
					Models.Order.create(newOrder, function(err, created) {
						if (!err) {
							res.sendStatus(200);
						} else {
							//there was an error creating order
							res.sendStatus(500);
						}
					});

				}
			});
		} else {
			//charge 15$ per meal

			brainTreeClient.transaction.sale({
				customerId: req.user.customerId,
				amount: (creates.length * 15).toString()
			}, function(err, result) {
				console.log(err, result);
				if (!err) {
					Models.Order.create(newOrder, function(err, created) {
						if (!err) {
							res.sendStatus(200);
						} else {
							//there was an error creating order
							res.sendStatus(500);
						}
					});
				}
			});

		}
		//creates should be ready now
		//for each create, add the item to addons
	});




	router.get('/between/:startDate/:endDate', passport.authenticate('bearer'), Models.Group.isAdmin, function(req, res) {
		var startDate = new Date(req.params.startDate).toISOString();
		var endDate = new Date(req.params.endDate).toISOString();
		Models.Order.OrdersBetweenDatesQuery(startDate, endDate, function(err, query) {
			query.exec(function(err, orders) {
				if (!err) {
					res.json({
						orders: orders
					});
				} else {
					res.status(500).send("There was a problem finding items between the given dates");
				}
			});
		});
	});

	router.get('/', passport.authenticate('bearer'), function(req, res) {
		Models.Order.find({
				userid: req.user._id,
			})
			.deepPopulate('items.entree items.side1 items.side2 items.entree.item items.side1.item items.side2.item')
			.exec(function(error, newOrders) {
				var newOrdersFlat = _.chain(newOrders).pluck('items').flatten().value();
				res.json({
					orders: newOrdersFlat
				});
			});
	});

	router.get('/deliver/:orderId', passport.authenticate('bearer'), Models.Group.isAdmin, function(req, res) {
		Models.Order.findByIdAndUpdate(req.params.orderId, {
			delivered: true
		}, function(err, modified) {
			if (!err) {
				brainTreeClient.transaction.sale({
					customerId: req.user.customerId,
					amount: '3.00'
				}, function(err, result) {
					if (!err) {
						res.sendStatus(200);
					} else {
						//there was an error creating order
						res.sendStatus(500);
					}
				});
			} else
				res.sendStatus(500);
		});
	});


	router.get('/count/menuItem/:id', /*passport.authenticate('bearer'), Models.Group.isAdmin,*/ function(req, res) {
		Models.Order.find({}, function(err, allOrders) {
			var allItems = _.chain(allOrders).pluck('items').flatten().value();
			var allEntrees = _.chain(allItems).pluck('entree').flatten().map(String).value();
			var allSide1 = _.chain(allItems).pluck('side1').flatten().map(String).value();
			var allSide2 = _.chain(allItems).pluck('side2').flatten().map(String).value();
			var allOrderedIds = allEntrees.concat(allSide1).concat(allSide2);
			var uniqOrderedIds = _.uniq(allOrderedIds);
			//find all the orderitems that were ordered, so we can get the menuItems
			Models.OrderItem.find({
				_id: {
					$in: uniqOrderedIds
				}
			}, function(err, allOrderedItems) {
				console.log(allOrderedItems);
				var allOrderedMenuItemIds = _.chain(allOrderedItems).map(function(item) {
					return _.chain(item).pick('item', '_id').mapObject(function(val, key) {
						//have to convert to string so we can compare the ids
						return val.toString();
					}).value();
				}).value();
				var groupedOrderedIds = _.groupBy(allOrderedIds, function(id) {
					var found = _.findWhere(allOrderedMenuItemIds, {
						_id: id
					});
					if (found)
						return found.item;
				});
				var countedMenuItems = _.mapObject(groupedOrderedIds, function(list, id) {
					return list.length;
				});
				res.json(countedMenuItems);
			});
		});
	});


	router.get('/count/orderItem/:id', /*passport.authenticate('bearer'), Models.Group.isAdmin,*/ function(req, res) {
		Models.Order.find({}, function(err, allOrders) {
			var allItems = _.chain(allOrders).pluck('items').flatten().value();
			var allEntrees = _.chain(allItems).pluck('entree').flatten().map(String).value();
			var allSide1 = _.chain(allItems).pluck('side1').flatten().map(String).value();
			var allSide2 = _.chain(allItems).pluck('side2').flatten().map(String).value();
			var allOrderedIds = allEntrees.concat(allSide1).concat(allSide2);
			var groupedOrderedIds = _.countBy(allOrderedIds);
			console.log(groupedOrderedIds);
			res.json(groupedOrderedIds);
		});


	});

	return router;
};
module.exports = handler;
