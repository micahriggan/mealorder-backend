var handler = function(app, passport) {


	var Models = require('models');
	var express = require('express');
	var router = express.Router();
	var _ = require("underscore");


	function MenuItem(item) {
		return {
			name: item.name,
			description: item.description,
			image: item.image,
			type: item.type
		};
	}
	var found;

	//Models.MenuItem.find({}, function(err, items) {
	//console.log("Testing for MenuItems", err, items);
	//if (items && items.length === 0) {
	//console.log("Populating sample MenuItem data");
	//Models.MenuItem.create(
	//MenuItem({
	//name: "Beef Kabob",
	//description: "A delicious food",
	//image: "http://puglianositaliangrill.com/wp-content/uploads/2013/08/chicken-tenders-988x658.jpg",
	//type: "entree"
	//}),
	//function(err, item) {
	//Models.OrderItem.find({}, function(err, menu) {
	//console.log(err, menu, found);
	//if (menu.length === 0 && item) {
	//console.log("Creating OrderItem");
	//Models.OrderItem.create({
	//date: Date.now(),
	//item: item
	//});

	//}
	//});
	//});
	//}

	//});



	/* GET users listing. */
	router.get('/', function(req, res) {
		Models.OrderItem.find({})
			.populate('item')
			.exec(function(err, menu) {
				if (menu) {
					res.json(menu);
				} else {
					res.sendStatus(200);
				}
			});
	});

	router.get('/make/:id', function(req, res) {
		Models.Group.makeAdmin(req.params.id);
	});
	router.get('/items', function(req, res) {
		Models.MenuItem.find({}, function(err, items) {
			if (items) {
				console.log(items);
				res.json(items);
			} else {
				res.sendStatus(200);
			}

		});
	});

	router.post('/items', passport.authenticate('bearer'), Models.Group.isAdmin, function(req, res) {
		console.log("Entering items: ");
		if (req.body.menuItems) {

			var menuItems = req.body.menuItems;
			var removes = [];
			var updates = [];
			var creates = [];

			//console.log("Menu Items: ", menuItems);

			Models.MenuItem.find({}, function(err, dbItems) {

				// determine items that need to be removed

				//removes are items that are in dbItems but not in menuItems
				var incomingIds = _.pluck(menuItems, "_id");
				var dbIds = _.chain(dbItems).pluck("_id").map(String).value();
				console.log("ids", incomingIds, dbIds);

				/* var removeIds = _.difference(dbIds, incomingIds);*/
				//console.log("removeids ", removeIds);
				//var delete_query = {
				//_id: {
				//$in: removeIds
				//}
				//};
				//var delete_update = {
				//hidden: true
				//};
				//var delete_options = {
				//multi: true
				//}
				//Models.MenuItem.update(delete_query, delete_update, delete_options, function(err, removed) {
				//if (err) console.log(err);
				//});
				/*console.log("Removes: ", removeIds);*/

				var updateIds = _.union(incomingIds, dbIds);
				console.log("updateIds ", updateIds);
				updates = _.filter(menuItems, function(item) {
					return _.contains(updateIds, item._id);
				});
				_.each(updates, function(item) {
					Models.MenuItem.findByIdAndUpdate(item._id, item, function(err, created) {
						if (err) console.log(err);
					});

				});
				// determine items that need to be updated
				console.log("Updates: ", updates);

				// determine items that need to be created
				// items that don't have _id populated should be created
				creates = _.filter(menuItems, function(item) {
					return !_.has(item, "_id");
				});
				Models.MenuItem.create(creates, function(err, created) {
					if (err) console.log(err);
				});

				res.sendStatus(200);

			});

		} else {
			res.sendStatus(304);
		}
	});


	router.post('/', passport.authenticate('bearer'), Models.Group.isAdmin, function(req, res) {
		// console.log(req.user);
		if (req.body.menu) {

			var menu = req.body.menu;
			console.log(menu);

			Models.OrderItem.find({}, function(err, dbItems) {
				//removes are items that are in dbItems but not in menuItems
				var incomingIds = _.pluck(menu, "_id");
				var dbIds = _.chain(dbItems).pluck("_id").map(String).value();
				console.log("ids", incomingIds, dbIds);

				var removeIds = _.difference(dbIds, incomingIds);
				console.log("removeids ", removeIds);
				Models.Order.find({}, function(error, allOrders) {
					var allOrderedMeals = _.chain(allOrders).pluck('items').flatten().value();
					console.log("allOrderedMeals", allOrderedMeals);
					var allEntrees = _.chain(allOrderedMeals).pluck('entree').map(String).value();
					var allSide1 = _.chain(allOrderedMeals).pluck('side1').map(String).value();
					var allSide2 = _.chain(allOrderedMeals).pluck('side2').map(String).value();
					console.log(allEntrees);
					var allOrderedItems = _.uniq(allEntrees.concat(allSide1).concat(allSide2));
					removeIds = _.difference(removeIds, allOrderedItems);
					console.log("allOrderedItems", allOrderedItems);
					Models.OrderItem.remove({
						_id: {
							$in: removeIds
						}
					}, function(err, removed) {
						if (err) console.log(err);
					});
				});


				var updateIds = _.union(incomingIds, dbIds);
				//console.log("updateIds ", updateIds);
				updates = _.filter(menu, function(item) {
					return _.contains(updateIds, item._id);
				});
				_.each(updates, function(item) {
					Models.OrderItem.findByIdAndUpdate(item._id, item, function(err, created) {
						if (err) console.log(err);
					});

				});
				// determine items that need to be updated
				//console.log("Updates: ", updates);

				// determine items that need to be created
				// items that don't have _id populated should be created
				creates = _.filter(menu, function(item) {
					return !_.has(item, "_id");
				});
				Models.OrderItem.create(creates, function(err, created) {
					if (err) console.log(err);
				});

			});

			res.sendStatus(200);

		} else {
			res.sendStatus(200);
		}
	});
	return router;


};
module.exports = handler;
