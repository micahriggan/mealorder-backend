var handler = function(app, passport) {

	var express = require('express');
	var router = express.Router();

	router.get('/', function(req, res) {
		res.send({
			user: req.user,
			session: req.session
		});
	});


	app.use('/contact', require('routes/contact')(app));
	//Routes that don't need passport

	app.use('/user', require('routes/user')(app, passport));
	app.use('/refer', require('routes/refer')(app, passport));
	app.use('/menu', require('routes/menu')(app, passport));
	app.use('/payment', require('routes/payment')(app, passport));
	app.use('/order', require('routes/order')(app, passport));
	app.use('/', router);

	return router;
};

module.exports = handler;
