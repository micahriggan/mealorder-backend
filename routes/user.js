var handler = function(app, passport) {
	var express = require('express');
	var router = express.Router();
	var models = require('models');
	var crypto = require('crypto');

	var brainTree = require('braintree');

	var brainTreeClient = brainTree.connect({
		environment: brainTree.Environment.Sandbox,
		merchantId: process.env.braintree_merchantid,
		publicKey: process.env.braintree_publickey,
		privateKey: process.env.braintree_privatekey,

	});


	//app.use("/", passport.authenticate('bearer'));
	// change this to use the router path

	/* GET users listing. */
	router.get('/', passport.authenticate('bearer'), function(req, res) {
		res.send(req.user);
	});

	router.post('/', function(req, res) {
		var user = req.body.user;
		console.log(req.body);
		var token = crypto.randomBytes(64).toString('hex');
		var randomNumber = crypto.randomBytes(2).toString('hex');
		models.User.create({
			firstname: user.firstname,
			lastname: user.lastname,
			username: user.username,
			email: user.email,
			referral: user.firstname + randomNumber,
			city: user.city,
			state: user.state,
			street: user.street,
			planId: user.planId,
			zip: user.zip,
			token: token
		}, function(err, user) {
			if (!err) {
				models.Password.createPassword(user.username, req.body.user.password,
					function() {
						console.log("Manually created ", user);
						var nonce = req.body.payment_method_nonce;
						// use the payment method nonce here
						// Create the user and add it to the user object
						brainTreeClient.customer.create({
							firstName: user.firstname,
							lastName: user.lastname,
							paymentMethodNonce: nonce
						}, function(err, result) {
							if (result.success) {
								console.log(result.customer.id);
								models.User.findByIdAndUpdate(user._id, {
									customerId: result.customer.id.toString()
								}, function(err, user) {
									console.log(err, user);
									if (user.planId !== "None") {
										brainTreeClient.subscription.create({
											planId: user.planId,
											paymentMethodToken: result.customer.paymentMethods[0].token
										}, function(err, result) {
											if (!err) {
												console.log(result);
												//subscription for planId has been created for user
												//Update user with subscrition and planid
												models.User.findByIdAndUpdate(user._id, {
														subscriptionId: result.subscription.id
													},
													function(err, user) {
														console.log("subscription created for " + user);

													});
											}
										});
									}
									res.json({
										token: token
									});

								});
							} else {
								res.status(500).send("There was a problem creating the braintree user");
							}
						});


					},
					function(error) {
						console.log(error);
						res.status(500).send("There was a problem creating the user");
					});
			} else {
				console.log(err);
				res.status(500).send("Username is already taken");
			}

		});

	});

	router.post('/update', passport.authenticate('bearer'), function(req, res) {
		if (req.user) {
			var user = req.body.user;
			console.log("Looking for user " + req.user._id);
			console.log("Updating with : ", user);
			models.User.findByIdAndUpdate(req.user._id, {
				firstname: user.firstname,
				lastname: user.lastname,
				email: user.email,
				city: user.city,
				state: user.state,
				street: user.street,
				zip: user.zip,

			}, function(err, user) {
				console.log("Modified user ", err, user);
				res.sendStatus(200);
			});
		} else res.sendStatus(304);
	});


	router.get('/profile-photo', function(req, res) {
		res.send(req.user);
	});
	router.get('/logout', passport.authenticate('bearer'), function(req, res) {
		if (req.user) {
			console.log('Logging out user');
			models.User.logout(req.user._id);
		}
		res.sendStatus(200);
	});

	return router;
};

module.exports = handler;
