var handler = function(app, passport) {



	var express = require('express');
	var router = express.Router();
	var brainTree = require('braintree');
	var Models = require('models');
	var ObjectId = require('mongoose').Types.ObjectId;

	var brainTreeClient = brainTree.connect({
		environment: brainTree.Environment.Sandbox,
		merchantId: process.env.braintree_merchantid,
		publicKey: process.env.braintree_publickey,
		privateKey: process.env.braintree_privatekey,

	});

	router.get('/token', function(req, res) {
		var token = "";
		brainTreeClient.clientToken.generate({
			//customerid: req.user._id
		}, function(err, response) {

			if (err) {
				console.log("error : ", err);
			} else {
				console.log(response);
				token = response.clientToken;
			}
			console.log("Sending token " + token);
			res.send({
				token: token
			});


		});
	});

	router.get('/customer', passport.authenticate('bearer'), function(req, res) {
		getBrainTreeUser(req.user, function(err, customer) {
			if (!err) {
				res.json({
					customer: customer
				});
			} else {
				res.sendStatus(500);
			}
		});

	});



	router.post('/customer', function(req, res) {
		//get user, check subscription level
		// if user doesn't have subscription, charge 15$ per meal
		//if number of orders is >= subscription number, then charge addon price
		// else charge nothing

		console.log("checkout user ");
		//create a braintree user and assign that id to the logged in user
		console.log(req.user);
		var nonce = req.body.payment_method_nonce;
		// use the payment method nonce here
		// Create the user and add it to the user object
		if (!req.user.customerId) {
			brainTreeClient.customer.create({
				firstName: req.user.firstName,
				lastName: req.user.lastName,
				paymentMethodNonce: nonce
			}, function(err, result) {
				if (result.success) {
					console.log(result.customer.id);
					Models.User.findByIdAndUpdate(req.user._id, {
						customerId: result.customer.id.toString()
					}, function(err, user) {
						console.log(err, user);
						res.sendStatus(200);
					});

				} else res.sendStatus(304);
			});
		} else {
			console.log("user already had braintreeId");
			res.sendStatus(200);
		}

	});

	router.get('/plans', function(req, res) {
		brainTreeClient.plan.all(function(err, result) {
			//console.log(result);
			res.json(result);
		});
		// res.sendStatus(200);
	});

	router.post('/subscribe/:planId', passport.authenticate('bearer'), function(req, res) {
		var subscriptionType = req.params.planId;
		getBrainTreeUser(req.user, function(err, customer) {
			if (!err) {
				console.log(customer);
				var newSub = {
					planId: subscriptionType,
					paymentMethodToken: customer.paymentMethods[0].token
				};
				var userUpdate = {
					planId: subscriptionType,
					subscriptionId: result.subscription.id
				};

				brainTreeClient.subscription.create(newSub, function(err, result) {
					if (!err) {
						console.log(result);
						//subscription for planId has been created for user
						//Update user with subscrition and planid
						Models.User.findByIdAndUpdate(req.user._id, userUpdate,
							function(err, user) {
								res.sendStatus(200);
							});
					} else {
						res.sendStatus(304);
					}
				});
			}
		});

		res.sendStatus(500);
	});
	router.get('/itemprice', passport.authenticate('bearer'), function(req, res) {
		Models.Order.GetOrderPrice(req.user, function(err, pricingInfo) {
			res.json({
				pricingInfo: pricingInfo
			});
		});
	});


	function getBrainTreeUser(userModel, callback) {
		brainTreeClient.customer.find(userModel.customerId, function(err, customer) {
			if (customer)
				callback(null, customer);
			else callback(err, null);
		});

	}

	return router;


};

module.exports = handler;
