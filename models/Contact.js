function contactModel(){
  var mongoose = require('mongoose');
  var Schema = mongoose.Schema;
  var ObjectId = Schema.ObjectId;

  var Contact = new Schema({
    userId: ObjectId,
    name: String,
    email: String,
    subject: String,
    message: String
  });

  return mongoose.model('Contact', Contact);

}

module.exports = contactModel();
