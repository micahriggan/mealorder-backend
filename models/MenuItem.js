function MenuItemModel() {


	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;


	var MenuItem = new Schema({
		id: ObjectId,
		name: String,
		description: String,
		image: String,
		type: String,
		hidden : {type: Boolean, default : false}
	});

	return mongoose.model('MenuItem', MenuItem);


}


module.exports = MenuItemModel();
