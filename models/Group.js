function GroupModel() {
	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;
	var deepPopulate = require('mongoose-deep-populate')(mongoose);


	var Group = new Schema({
		group: String,
		userid: {
			type: ObjectId,
			ref: 'User'
		}
	});

	Group.plugin(deepPopulate, {
		whiteList: ['userid']
	});
	Group.statics.makeAdmin = function(userid) {
		console.log("Making " + userid + " an admin");
		mongoose.model('Group', Group).create({
			group: 'Admin',
			userid: userid
		}, console.log);

	};
	Group.statics.isAdmin = function(req, res, next) {
		console.log("checking for admin privelages", req.user);
		if (!req.user) res.sendStatus(401);
		console.log("Finding user");
		mongoose.model('Group', Group).find({
			userid: req.user._id,
			group: "Admin"
		}, function(err, groups) {
			console.log("Finished find");
			console.log(err, groups);
			if (err) res.sendStatus(401);
			else if (!groups || groups.length === 0) res.sendStatus(401);
			else next();
		});


	};
	return mongoose.model('Group', Group);

}

module.exports = GroupModel();
