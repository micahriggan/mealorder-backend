function Models() {
	return {
		User: require('models/User'),
		Group: require('models/Group'),
		Contact: require('models/Contact'),
		OrderItem: require('models/OrderItem'),
		MenuItem: require('models/MenuItem'),
		Order : require('models/Order'),
		Password : require('models/Password')
	};
}
module.exports = Models();
