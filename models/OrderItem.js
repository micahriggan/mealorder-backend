function OrderItemModel() {
	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;
	var deepPopulate = require('mongoose-deep-populate')(mongoose);


	var OrderItem = new Schema({
		id: ObjectId,
		date: Date,
		item: {
			type: ObjectId,
			ref: 'MenuItem'
		}
	});

	OrderItem.plugin(deepPopulate, {
		whiteList: ['item']
	});
	return mongoose.model('OrderItem', OrderItem);

}

module.exports = OrderItemModel();
