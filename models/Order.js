function OrderModel() {
	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;
	var deepPopulate = require('mongoose-deep-populate')(mongoose);
	var _ = require('underscore');
	var Meal = new Schema({
		entree: {
			type: ObjectId,
			ref: 'OrderItem'
		},
		side1: {
			type: ObjectId,
			ref: 'OrderItem'
		},
		side2: {
			type: ObjectId,
			ref: 'OrderItem'
		}
	});

	var Order = new Schema({
		id: ObjectId,
		user: {
			type: ObjectId,
			ref: 'User'
		},
		items: [Meal],
		delivered: {
			type: Boolean,
			default: false
		},
		address: String
	});



	Order.plugin(deepPopulate, {
		whiteList: [
			'items', 'user'
		]
	});

	//this function retrieves the query object for dates between a range
	Order.statics.OrdersBetweenDatesQuery = function(startDate, endDate, callback) {
		var startDateStr = new Date(startDate).toISOString();
		var endDateStr = new Date(endDate).toISOString();
		console.log("finding orders between", startDateStr, endDateStr);
		mongoose.models.OrderItem.where('date').gte(startDateStr).lte(endDateStr).select('_id').exec(function(err, orderItems) {
			var query = mongoose.model('Order', Order).where('items.entree').in(orderItems)
				.deepPopulate('items.entree items.side1 items.side2 items.entree.item items.side1.item items.side2.item user.ref');
			query.exec(function(err, res) {
				console.log(err, res);
			});
			callback(err, query);

		});
	};


	//this function tells how much a user should pay for their order
	Order.statics.GetOrderPrice = function(user, callback) {
		var date = new Date();
		var beginOfMonth = new Date(date.getFullYear(), date.getMonth(), 1);
		var endOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
		mongoose.model('Order', Order).OrdersBetweenDatesQuery(beginOfMonth, endOfMonth, function(err, query) {
			query.where('user', user._id)
				.exec(function(err, myOrders) {
					console.log(err, myOrders);
					var allOrders = _.chain(myOrders).pluck('items').flatten().value();
					var price = 0;
					var ordersThisMonth = allOrders.length;
					var subscriptionMealsLeft = 0;
					var subscriptionMealsTotal = 0;
					var addonPrice = 15;
					console.log(user);
					if (user.planId && user.subscriptionId) {
						if (user.planId == "Premium") {
							subscriptionMealsTotal = 4;
							subscriptionMealsLeft = subscriptionMealsTotal - ordersThisMonth;
							addonPrice = 10;
							price = subscriptionMealsLeft > 0 ? 0 : addonPrice;
						} else if (user.planId == "Valued") {
							subscriptionMealsTotal = 2;
							subscriptionMealsLeft = subscriptionMealsTotal - ordersThisMonth;
							addonPrice = 12;
							price = subscriptionMealsLeft > 0 ? 0 : addonPrice;
						}
						subscriptionMealsLeft = subscriptionMealsLeft < 0 ? 0 : subscriptionMealsLeft;
						var pricingInfo = {
							price: price,
							ordersThisMonth: ordersThisMonth,
							subscriptionMealsTotal: subscriptionMealsTotal,
							subscriptionMealsLeft: subscriptionMealsLeft,
							addonPrice: addonPrice
						};
						callback(err, pricingInfo);


					} else {
						//charge 15$ per meal
						var pricingInfo = {
							price: 15,
							ordersThisMonth: ordersThisMonth,
							subscriptionMealsTotal: subscriptionMealsTotal,
							subscriptionMealsLeft: subscriptionMealsLeft,
							addonPrice: addonPrice
						};
						callback(err, pricingInfo);



					}


				});
		});


	};

	return mongoose.model('Order', Order);

}

module.exports = OrderModel();
