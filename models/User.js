function userModel() {
	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;

	var User = new Schema({
		id: ObjectId,
		username: {
			type: String,
			index: {
				unique: true
			}
		},
		email: String,
		firstname: String,
		lastname: String,
		city: String,
		state: String,
		street: String,
		zip: String,
		profileId: String,
		referral: {
			type: String,
			lowercase: true
		},
		token: String,
		refferedBy: {
			type: String,
			lowercase: true
		},
		subscriptionId: String,
		planId: String,
		customerId: String
	});
	User.statics.logout = function(id) {
		mongoose.model('User', User).findByIdAndUpdate(id, {
			token: null
		}, function(err, user) {
			console.log('Attempting to logout user : ' + user);

		});
	};

	return mongoose.model('User', User);

}

module.exports = userModel();
