function passwordModel() {
	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
	var bcrypt = require('bcryptjs');



	var Password = new Schema({
		username: String,
		hash: String
	});

	Password.statics.checkPassword = function(username, password, done) {
		this.model('Password').findOne({
				username: username
			},
			function(err, found) {
				console.log(err, found);	
				
				if (err) {
					done(false);
				}
				if (!found) {
					done(false);
				}
				done(bcrypt.compareSync(password, found.hash));

			});
	};

	Password.statics.createPassword = function(username, password, callback, error) {
		this.model('Password').create({
			username: username,
			hash : bcrypt.hashSync(password, 8)
		}, function(err, password) {
			if(!err){
				console.log("password created for user " , username, err);
				callback();
			}
			else error(err);
		});

	};

	return mongoose.model('Password', Password);

}

module.exports = passwordModel();
